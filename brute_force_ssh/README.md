### Description

    This Python script is a basic SSH password cracker

### Prerequisites

    Python 3.11-venv (linux)

### Linux Setup

### 1 - Clone the repository:

    git clone https://gitlab.com/python7172650/hacking/tools_hacking_python.git

### 2 - Navigate to the cloned directory :

    cd tools_hacking_python; cd brute_force_ssh

### Create a virtual environment (optional, but recommended):

    python3 -m venv ssh

### Activate environement virtual

    source ssh/bin/activate

### Install required Python packages:

    sudo pip install -r requirements.txt


### To run the script :

    python3 brute_force_ssh.py 192.168.1.10 john /usr/share/wordlists/rockyou.txt

### Limitations

    This script is a very basic implementation of an SSH password cracker and may not be effective 
    against complex passwords or secure SSH configurations. 
    It is recommended to use a more advanced tool or technique for more complex SSH attacks.

### Note

    This script is intended for educational and ethical purposes only. Do not use it to scan networks without proper authorization.

### License

    This project is open source and available under the MIT License.
### Description

This Python script performs an ARP scan on a network using Scapy library. 
It sends an ARP request to a range of IP addresses to discover the MAC addresses of active hosts.
That root privileges are required to send ARP requests.

### Prerequisites

    Python 3.11-venv (linux)


### Linux Setup

### 1 - Clone the repository:

    git clone https://gitlab.com/python7172650/hacking/tools_hacking_python.git


### 2 - Navigate to the cloned directory :

    cd tools_hacking_python; cd scan_arp_network

### Create a virtual environment (optional, but recommended):

    python3 -m venv ARP

### Activate environement virtual

    source ARP/bin/activate

### Install required Python packages:

    sudo pip install -r requirements.txt

### To run the script :

    sudo python3 scan_arp_network.py -i eth0 -r 192.168.1.0/24

### Notes

    The timeout parameter in srp() sets the maximum time to wait for a response (in seconds).
    The inter parameter in srp() sets the time to wait between sending each packet (in seconds).
    This script is intended for educational and ethical purposes only. Do not use it to scan networks without proper authorization.

### Contributing

    ontributions to this project are welcome! Please feel free to fork the repository, make your changes, 
    and create a pull request. Authors and Acknowledgment

### License

    This project is open source and available under the MIT License.

import sys
import hashlib

wordlist_location = sys.argv[1]
hash_input = sys.argv[2]

with open(wordlist_location, 'rb') as file:  # Ouverture en mode binaire
    for line in file:
        # Tentative de décodage de la ligne, en ignorant les erreurs de décodage
        try:
            # Décode chaque ligne individuellement
            line = line.strip().decode('utf-8')
        except UnicodeDecodeError:
            # Si une erreur de décodage se produit, ignore la ligne
            continue

        hash_ob = hashlib.md5(line.encode('utf-8'))
        hashed_pass = hash_ob.hexdigest()
        if hashed_pass == hash_input:
            print('Found cleartext password! : ' + line)
            exit(0)


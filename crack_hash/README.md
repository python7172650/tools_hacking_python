### Description

    This Python script is a basic hash cracker for MD5 hashes. The script takes in a wordlist file location 
    and a hash to be cracked as inputs. 
    The script then reads each line of the wordlist file, hashes the line using MD5 algorithm and 
    compares it with the provided hash. 
    If a match is found, the cleartext password is printed to the console.

### Prerequisites

    Python 3.11-venv (linux)

### Linux Setup

### 1 - Clone the repository:

    git clone https://gitlab.com/python7172650/hacking/tools_hacking_python.git

### 2 - Navigate to the cloned directory :

    cd tools_hacking_python; cd crack_hash

### Create a virtual environment (optional, but recommended):

    python3 -m venv crack

### Activate environement virtual

    source crack/bin/activate

### Install required Python packages:

    sudo pip install -r requirements.txt

### To run the script :

    python3 crack_hash.py /usr/share/wordlists/rockyou.txt 5d41402abc4b2a76b9719d911017c592

### Limitations

    This script is a very basic implementation of a hash cracker and may not be effective against complex hashes or strong passwords. 
    It is recommended to use a more advanced tool or technique for more complex hashes. Additionally, the script is currently limited to only cracking MD5 hashes. 

### Note

    This script is intended for educational and ethical purposes only. Do not use it to scan networks without proper authorization.

### License

    This project is open source and available under the MIT License.
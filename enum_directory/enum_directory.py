import requests
import sys


from requests.packages.urllib3.exceptions import InsecureRequestWarning

# Disable the InsecureRequestWarning from urllib3
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

# Check if we should ignore SSL certificate errors.
ignore_ssl = False
if "--ignore-ssl" in sys.argv:
    ignore_ssl = True
    sys.argv.remove("--ignore-ssl")

# Ensure that a domain or IP address is passed as a command-line argument.
if len(sys.argv) != 2:
    print("Usage: python script.py <domain-or-ip> [--ignore-ssl]")
    sys.exit(1)

# Read the wordlist and split it into lines.
sub_list = open("wordlist.txt").read()
directories = sub_list.splitlines()

# Retrieve the domain or IP from the command line arguments.
domain_or_ip = sys.argv[1]

# Check if the domain or IP starts with 'http://' or 'https://', if not, prepend 'http://'.
if not (domain_or_ip.startswith('http://') or domain_or_ip.startswith('https://')):
    domain_or_ip = 'http://' + domain_or_ip

# Loop over each directory in the wordlist.
for dir in directories:
    # Test each directory with both .html and .php extensions.
    for extension in ['.html', '.php']:
        # Construct the URL properly by including a slash between the domain and the path.
        dir_enum = f"{domain_or_ip}/{dir}{extension}"
        try:
            # Pass the 'verify' parameter based on the ignore_ssl flag
            r = requests.get(dir_enum, verify=not ignore_ssl)
            if r.status_code != 404:  # If the status code is not 404, it might be a valid directory.
                print("Valid directory:", dir_enum)
        except requests.ConnectionError as e:
            print(f"Failed to connect to {dir_enum}")
            print(f"Error: {e}")

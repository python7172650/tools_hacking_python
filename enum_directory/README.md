### Description

This Python script checks for valid directories on a web server using a provided list of directories 
and a target URL.

### Prerequisites

    Python 3.11-venv (linux)

### Linux Setup

### 1 - Clone the repository:

    git clone https://gitlab.com/python7172650/hacking/tools_hacking_python.git

### 2 - Navigate to the cloned directory :

    cd tools_hacking_python; cd enum_directory

### Create a virtual environment (optional, but recommended):

    python3 -m venv enum

### Activate environement virtual

    source enum/bin/activate

### Install required Python packages:

    sudo pip install -r requirements.txt


### To run the script :

    python3 enum_directory.py 54.239.28.85 --ignore-ssl (optional)

### Notes

    The script will ignore any directories that return a 404 status code.
    This script is intended for educational and ethical purposes only. Do not use it to scan networks without proper authorization.

### License

    This project is open source and available under the MIT License.

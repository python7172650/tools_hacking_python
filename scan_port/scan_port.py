import sys
import socket

# Vérifiez si les arguments sont fournis
if len(sys.argv) < 4:
    print("Usage: python script.py <ip> <start_port> <end_port>")
    sys.exit(1)

ip = sys.argv[1]
start_port = int(sys.argv[2])
end_port = int(sys.argv[3])

# Assurez-vous que la plage de ports est valide
if start_port < 1 or end_port > 65535 or end_port < start_port:
    print("Invalid port range. Ports should be between 1 and 65535 and end_port should be greater than or equal to start_port.")
    sys.exit(1)

open_ports = []

def probe_port(ip, port, result = 1):
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(0.5)
        r = sock.connect_ex((ip, port))
        if r == 0:
            result = r
        sock.close()
    except Exception as e:
        pass
    return result

for port in range(start_port, end_port + 1):
    sys.stdout.flush()
    response = probe_port(ip, port)
    if response == 0:
        open_ports.append(port)

if open_ports:
    print("Open Ports are: ")
    print(sorted(open_ports))
else:
    print("Looks like no ports are open :(")

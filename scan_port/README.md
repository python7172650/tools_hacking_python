### Description

This Python script performs a TCP port scan on a target IP address to discover open ports. 
It uses socket library to create a TCP socket and attempts to connect to each port in a range of 1-65535. 
If a connection is successful, it is assumed that the port is open.

### Prerequisites

    Python 3.11-venv (linux)

### Linux Setup

### 1 - Clone the repository:

    git clone https://gitlab.com/python7172650/hacking/tools_hacking_python.git


### 2 - Navigate to the cloned directory :

    cd tools_hacking_python; cd scan_port

### Create a virtual environment (optional, but recommended):

    python3 -m venv PORT

### Activate environement virtual

    source PORT/bin/activate

### Install required Python packages:

    sudo pip install -r requirements.txt

### To run the script :

    sudo python3 scan_port 192.168.1.0 1 100 (scan range 1-100)

### Notes
 
    If the connection is successful, the function returns 0 to indicate that the port is open.
    This script is intended for educational and ethical purposes only. 
    Do not use it to scan networks without proper authorization.

### Contributing

    ontributions to this project are welcome! Please feel free to fork the repository, make your changes, 
    and create a pull request. Authors and Acknowledgment

### License

    This project is open source and available under the MIT License.
